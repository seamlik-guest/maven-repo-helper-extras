This project is a new approach to extend
[`maven-repo-helper`](https://tracker.debian.org/pkg/maven-repo-helper). The main differences
include:

  * Uses upstream code for jobs like resolving Maven dependencies and parsing `d/control`
  * Installs Maven artifacts to the system and exposes its API and Javadoc

The following sections will describe these utilities in details.

mh_shlibdeps
============

This program is designed like a `debhelper` plugin and works just like
[`dh_shlibdeps`](https://manpages.debian.org/testing/debhelper/dh_shlibdeps.1.en.html). It examines
the Maven artifacts installed by a binary package and generates `${maven:Depends}` in the
corresponding `<package>.substvars` files.

Some facts should be noted:

  * Ignores all optional dependencies.

mh_genlaunchers
===============

This program reads a `<package>.maven-launchers` file and generates launcher scripts. The file has
a simple syntax like:

```
<launcher path>   <main class name>   <artifact>
# Comments
```

For example, in order to install a launcher script at `/usr/bin/mh_genlaunchers` which loads a class
`org.debian.maven.Genlaunchers` which resides in a Maven artifact
`org.debian:maven-repo-helper-extras:debian`, one may use:

```
usr/bin/mh_shlibdeps    org.debian.maven.Shlibdeps   org.debian:maven-repo-helper-extras:debian
```

The program will resolve all transitive dependencies of this JAR, generates a classpath list, and
write a launcher script to the chosen location.

How to Use
----------

In `d/rules`:

```make
%:
	dh $@ --with maven_repo_helper_extras --buildsystem=<...>
```

This Debhelper plugin installs `maven-repo-helper` as well, since it is the one who installs
all those Maven artifacts.