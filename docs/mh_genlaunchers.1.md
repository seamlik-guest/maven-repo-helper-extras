% mh_genlaunchers(1) maven-repo-helper-extras

# NAME

mh_genlaunchers - generates launcher scripts based on Maven artifact metadata

# SYNOPSIS

**mh_genlaunchers** [_shared-debhelper-options_] [_options_]

# OPTIONS

--repo=
: Specify the Maven repository of both the host system and the package content to search. Should be
a relative path.

--help | -h | -?
: Show help.

# SEE ALSO

debhelper(7)