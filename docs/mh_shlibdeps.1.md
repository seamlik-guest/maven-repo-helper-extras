% mh_shlibdeps(1) maven-repo-helper-extras

# NAME

mh_shlibdeps - calculates Maven artifact dependencies for a Debian package

# SYNOPSIS

**mh_shlibdeps** [_shared-debhelper-options_] [_options_]

# OPTIONS

--repo=
: Specify the Maven repository of both the host system and the package content to search. Should be
a relative path.

--help | -h | -?
: Show help.

# SEE ALSO

debhelper(7)