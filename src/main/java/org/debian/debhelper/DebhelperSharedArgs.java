/*
 * Copyright 2018 殷啟聰 | Kai-Chung Yan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debian.debhelper;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * JCommander object for
 * <a href="https://manpages.debian.org/testing/debhelper/debhelper.7.en.html">shared
 * {@code debhelper} options</a>.
 */
@Parameters(separators = "=", hidden = true)
public class DebhelperSharedArgs {

  @Parameter(names = { "-v", "--verbose" })
  private boolean verbose;

  @Parameter(names = "--no-act")
  private boolean noAct;

  @Parameter(names = { "-a", "--arch " })
  private boolean arch;

  @Parameter(names = { "-i", "--indep" })
  private boolean indep;

  @Parameter(names = { "-p", "--package" })
  private List<String> packages;

  @Parameter(names = { "-N", "--no-package" })
  private List<String> noPackages;

  @Parameter(names = "--remaining-packages")
  private boolean remainingPackges;

  @Parameter(names = "--ignore")
  private List<String> ignore;

  @Parameter(names = { "-P", "--tmpdir" })
  private Path tmpdir;

  @Parameter(names = "--main-package")
  private String mainPackage;

  public List<String> getIncludedPackages() {
    return packages == null ? Collections.emptyList() : packages;
  }

  /**
   * Refers to {@code --verbose}.
   */
  public boolean isVerbose() {
    return verbose;
  }

  /**
   * Refers to {@code --arch}.
   */
  public boolean isArch() {
    return arch;
  }

  /**
   * Refers to {@code --indep}.
   */
  public boolean isIndep() {
    return indep;
  }

  /**
   * Refers to {@code --no-act}.
   */
  public boolean isNoAct() {
    return noAct;
  }

  /**
   * Refers to {@code --main-package}.
   */
  public String getMainPackage() {
    return mainPackage == null ? "" : mainPackage;
  }

  /**
   * Refers to {@code --tmpdir}.
   */
  public Path getTmpdir() {
    return tmpdir;
  }

  /**
   * Refers to {@code --no-packages}.
   */
  public List<String> getExcludedPackages() {
    return noPackages == null ? Collections.emptyList() : noPackages;
  }

  /**
   * Refers to {@code --remaining-packages}.
   */
  public boolean isRemainingPackgesOnly() {
    return remainingPackges;
  }

  /**
   * Refers to {@code --ignore}.
   */
  public List<String> getIgnored() {
    return ignore == null ? Collections.emptyList() : ignore;
  }
}