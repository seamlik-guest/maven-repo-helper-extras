/*
 * Copyright 2018 殷啟聰 | Kai-Chung Yan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debian.debhelper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Contains util methods for working with debhelper.
 */
public class DebhelperUtils {

  public static Path DEFAULT_MAVEN_REPO_PATH = Paths.get("usr/share/maven-repo");

  /**
   * Checks if a directory is a "debian" directory in a source tree. Such directory must contain
   * files like {@code control}, {@code changelog}, etc..
   */
  public static boolean verifyDebianDirectory(final Path directory) {
    if (!Files.isDirectory(directory)) {
      return false;
    }
    final List<Path> essentials = new ArrayList<>();
    essentials.add(directory.resolve("changelog"));
    essentials.add(directory.resolve("control"));
    essentials.add(directory.resolve("copyright"));
    essentials.add(directory.resolve("rules"));
    return essentials.parallelStream().allMatch(Files::exists);
  }

  /**
   * Finds a Debian source tree starting from {@code path} and keep search towards the root. The
   * judgement is based on if a directory contains a "debian" directory with the help of
   * {@link #verifyDebianDirectory(Path)}.
   * @throws FileNotFoundException If {@code path} is not in a Debian source tree.
   */
  public static Path findDebianSourceTreeRoot(final Path path) throws IOException {
    final Path debianPath = path.resolve("debian");
    if (Files.exists(debianPath) && verifyDebianDirectory(debianPath)) {
      return path.toRealPath();
    } else if (path.toRealPath().getParent() == null) {
      throw new FileNotFoundException("You are not in a Debian source directory.");
    } else {
      return findDebianSourceTreeRoot(path.toRealPath().getParent());
    }
  }

  /**
   * Runs a command and returns the output.
   * @param directory If not {@code null}, sets the working directory for the command.
   * @throws IOException If any errors occurred during the child process's creation and runtime.
   */
  public static CommandResult runCommand(final Path directory, final List<String> command)
      throws IOException, InterruptedException {
    final ProcessBuilder builder = new ProcessBuilder(command);
    if (directory != null) {
      builder.directory(directory.toFile());
    }
    builder.redirectError(ProcessBuilder.Redirect.INHERIT);
    final Process process = builder.start();
    final List<String> output = new ArrayList<>();
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(process.getInputStream())
    )) {
      reader.lines().forEach(output::add);
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
    return new CommandResult(process.waitFor(), output);
  }

  /**
   * Searches for packages containing files matching {@code pattern}.
   * @throws IOException If fails to run {@code dpkg}.
   */
  public static Set<String> findPackageUsingDpkg(final String pattern)
      throws IOException, InterruptedException {
    final CommandResult commandResult = DebhelperUtils.runCommand(
        null,
        Arrays.asList("dpkg", "--search", pattern)
    );
    if (commandResult.getExitCode() != 0) {
      return Collections.emptySet();
    } else {
      return commandResult
          .getOutput()
          .parallelStream()
          .map(it -> it.split(":")[0])
          .collect(Collectors.toSet());
    }
  }

  /**
   * Lists binary packages using {@code dh_listpackages}.
   * @param sourceRoot Root of the source tree of a Debian source package.
   * @param arch Includes arch-dependent packages only. ({@code --arch}).
   * @param indep Includes arch-independent packages only. ({@code --indep}).
   * @param excludes Excludes these packages ({@code --no-package}).
   * @param includes Includes these packages ({@code --package}).
   * @throws IOException If fails to run {@code dh_listpackages}.
   */
  public static List<String> listPackages(final Path sourceRoot,
                                          final boolean arch,
                                          final boolean indep,
                                          final Collection<String> excludes,
                                          final Collection<String> includes)
      throws IOException, InterruptedException {
    final List<String> command = new ArrayList<>();
    command.add("dh_listpackages");
    if (arch) {
      command.add("--arch");
    }
    if (indep) {
      command.add("--indep");
    }
    excludes.forEach(it -> command.add("--no-package=" + it));
    includes.forEach(it -> command.add("--package=" + it));
    return DebhelperUtils
        .runCommand(sourceRoot, command)
        .getOutput()
        .parallelStream()
        .collect(Collectors.toList());
  }
}