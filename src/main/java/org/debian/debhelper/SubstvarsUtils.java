/*
 * Copyright 2018 殷啟聰 | Kai-Chung Yan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debian.debhelper;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.io.IOUtils;

/**
 * Utility methods for manipulating {@code substvars} files.
 */
public class SubstvarsUtils {

  /**
   * Reads a substvars file.
   * @param source Will be closed once this method finishes.
   */
  public static Map<String, String> read(final Reader source)
      throws IOException, InvalidFileFormatException {
    final Map<String, String> substvars = new LinkedHashMap<>();
    try (final LineNumberReader reader = new LineNumberReader(source)) {
      String line;
      while ((line = reader.readLine()) != null) {
        line = line.trim();
        if (line.startsWith("#") || line.isEmpty()) {
          continue;
        } else if (line.startsWith("=")) {
          throw new InvalidFileFormatException(String.format(
              "Line %1s: `=` is the first character.",
              reader.getLineNumber()
          ));
        }
        if (!line.contains("=")) {
          throw new InvalidFileFormatException(String.format(
              "Line %1s: `=` is absent.",
              reader.getLineNumber()
          ));
        }
        final List<String> pair = Stream
            .of(line.split("="))
            .map(String::trim)
            .collect(Collectors.toList());
        if (pair.size() > 2) {
          throw new InvalidFileFormatException(String.format(
              "Line %1s: Multiple `=` present.",
              reader.getLineNumber()
          ));
        } else if (substvars.containsKey(pair.get(0))) {
          throw new InvalidFileFormatException(String.format(
              "Line %1s: `%2s` already defined.",
              reader.getLineNumber(),
              pair.get(0)
          ));
        } else {
          substvars.put(
              pair.get(0),
              pair.size() == 2 ? pair.get(1) : ""
          );
        }
      }
    }
    return substvars;
  }

  /**
   * Writes to a {@code substvars} file.
   * @param output Will be closed once this method finishes.
   */
  public static void write(final Map<String, String> data, final Writer output) throws IOException {
    try (final BufferedWriter writer = IOUtils.buffer(output)) {
      for (Map.Entry<String, String> entry : data.entrySet()) {
        writer.write(entry.getKey());
        writer.write('=');
        writer.write(entry.getValue());
        writer.newLine();
      }
    }
  }

  private SubstvarsUtils() {}
}