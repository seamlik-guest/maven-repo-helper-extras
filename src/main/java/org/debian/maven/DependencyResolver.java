/*
 * Copyright 2018 殷啟聰 | Kai-Chung Yan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debian.maven;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.maven.repository.internal.MavenRepositorySystemUtils;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifactType;
import org.eclipse.aether.collection.CollectRequest;
import org.eclipse.aether.collection.CollectResult;
import org.eclipse.aether.collection.DependencyCollectionException;
import org.eclipse.aether.connector.basic.BasicRepositoryConnectorFactory;
import org.eclipse.aether.graph.Dependency;
import org.eclipse.aether.graph.DependencyFilter;
import org.eclipse.aether.graph.DependencyNode;
import org.eclipse.aether.impl.DefaultServiceLocator;
import org.eclipse.aether.repository.ArtifactRepository;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.DependencyRequest;
import org.eclipse.aether.resolution.DependencyResolutionException;
import org.eclipse.aether.spi.connector.RepositoryConnectorFactory;
import org.eclipse.aether.spi.connector.layout.RepositoryLayout;
import org.eclipse.aether.spi.connector.layout.RepositoryLayoutProvider;
import org.eclipse.aether.spi.connector.transport.TransporterFactory;
import org.eclipse.aether.transfer.NoRepositoryLayoutException;
import org.eclipse.aether.transport.file.FileTransporterFactory;
import org.eclipse.aether.util.artifact.JavaScopes;
import org.eclipse.aether.util.artifact.OverlayArtifactTypeRegistry;
import org.eclipse.aether.util.filter.ScopeDependencyFilter;
import org.eclipse.aether.util.graph.selector.AndDependencySelector;
import org.eclipse.aether.util.graph.selector.OptionalDependencySelector;
import org.eclipse.aether.util.graph.selector.ScopeDependencySelector;

/**
 * Dependency resolver utilizing <a href="https://maven.apache.org/resolver">Maven Resolver</a>.
 */
public class DependencyResolver {

  private static final DependencyFilter DEPENDENCY_FILTER = new ScopeDependencyFilter(
      JavaScopes.TEST
  );

  private final RepositorySystem system;
  private final DefaultRepositorySystemSession session;
  private final RepositoryLayoutProvider layoutProvider;

  /**
   * Default constructor.
   * @param tmpRepo A temporary directory used as a local repository.
   * @throws IOException If failed to initialize {@code tmpRepo}.
   */
  public DependencyResolver(final Path tmpRepo) throws IOException {
    final DefaultServiceLocator locator = MavenRepositorySystemUtils.newServiceLocator();
    locator.addService(RepositoryConnectorFactory.class, BasicRepositoryConnectorFactory.class);
    locator.addService(TransporterFactory.class, FileTransporterFactory.class);

    system = locator.getService(RepositorySystem.class);
    session = MavenRepositorySystemUtils.newSession();
    layoutProvider = locator.getService(RepositoryLayoutProvider.class);

    session.setDependencySelector(new AndDependencySelector(
        new OptionalDependencySelector(),
        new ScopeDependencySelector(JavaScopes.TEST)
    ));
    session.setIgnoreArtifactDescriptorRepositories(true);

    final OverlayArtifactTypeRegistry registry = new OverlayArtifactTypeRegistry(
        session.getArtifactTypeRegistry()
    );
    registry.add(new DefaultArtifactType("aar"));
    session.setArtifactTypeRegistry(registry);

    Files.createDirectories(tmpRepo);
    session.setLocalRepositoryManager(system.newLocalRepositoryManager(
        session,
        new LocalRepository(tmpRepo.toFile())
    ));
  }

  /**
   * Resolves the runtime classpath of an Maven artifact.
   */
  public Collection<ArtifactResult>
  resolveAllDependencies(final Artifact artifact, final List<RemoteRepository> repositories)
      throws DependencyResolutionException, IOException {
    /* Caching the artifacts makes the result useless. */
    FileUtils.cleanDirectory(session.getLocalRepository().getBasedir());

    final CollectRequest collectRequest = new CollectRequest(
        new Dependency(artifact, JavaScopes.RUNTIME),
        repositories
    );
    final DependencyRequest dependencyRequest = new DependencyRequest(
        collectRequest,
        DEPENDENCY_FILTER
    );
    return system.resolveDependencies(session, dependencyRequest).getArtifactResults();
  }

  /**
   * Resolves direct dependencies.
   */
  public Collection<ArtifactResult>
  resolveDirectDependencies(final Artifact artifact, final List<RemoteRepository> repositories)
      throws IOException, DependencyCollectionException, ArtifactResolutionException {
    /* Caching the artifacts makes the result useless. */
    FileUtils.cleanDirectory(session.getLocalRepository().getBasedir());

    final CollectRequest request = new CollectRequest(
        new Dependency(artifact, JavaScopes.RUNTIME),
        repositories
    );
    final Collection<DependencyNode> dependencies = system
        .collectDependencies(session, request)
        .getRoot()
        .getChildren();
    final List<ArtifactResult> results = new ArrayList<>(dependencies.size());
    for (DependencyNode node : dependencies) {
      results.add(system.resolveArtifact(
          session,
          new ArtifactRequest(node)
      ));
    }
    return results;
  }

  /**
   * Resolves the relative path of the artifact inside a repository.
   */
  public Path resolveArtifactPath(final Artifact artifact, final ArtifactRepository repository) {
    if (repository instanceof LocalRepository) {
      return Paths.get(session.getLocalRepositoryManager().getPathForLocalArtifact(artifact));
    }
    try {
      final RepositoryLayout layout = layoutProvider.newRepositoryLayout(
          session,
          (RemoteRepository) repository
      );
      return Paths.get(layout.getLocation(artifact, false).getSchemeSpecificPart());
    } catch (NoRepositoryLayoutException ex) {
      throw new RuntimeException(ex);
    }
  }
}