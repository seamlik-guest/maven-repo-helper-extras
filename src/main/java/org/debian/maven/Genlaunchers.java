/*
 * Copyright 2018 殷啟聰 | Kai-Chung Yan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debian.maven;

import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.EnumSet;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.debian.debhelper.InvalidFileFormatException;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.resolution.DependencyResolutionException;

/**
 * Main entry point for {@code mh_genlaunchers}.
 */
@ProgramName("mh_genlaunchers")
public class Genlaunchers extends Mh {

  /**
   * Entry point.
   */
  public static void main(final String[] args) {
    new Genlaunchers().start(args);
  }

  @Override
  protected void run() {
    try {
      for (String pkg : determinePackagesToActOn()) {
        processPackage(pkg);
      }
    } catch (Exception ex) {
      getLogger().log(Level.SEVERE, null, ex);
      System.exit(1);
    }
  }

  private void processPackage(final String pkg) throws
      IOException,
      InvalidFileFormatException,
      DependencyResolutionException,
      InterruptedException {
    getLogger().info("Processing " + pkg);

    final Path namelessConfigPath = findSourceRoot().resolve("debian").resolve("maven-launchers");
    final Path configPath;
    if (determineMainPackage().equals(pkg) && Files.exists(namelessConfigPath)) {
      configPath = namelessConfigPath;
    } else {
      configPath = findSourceRoot().resolve("debian").resolve(pkg + ".maven-launchers");
    }
    final boolean configIgnored = findAllIgnoredFiles().stream().anyMatch(it -> {
      try {
        return Files.isSameFile(it, configPath);
      } catch (IOException ex) {
        getLogger().log(Level.SEVERE, null, ex);
        System.exit(1);
        return false;
      }
    });
    if (Files.notExists(configPath)) {
      return;
    }
    if (configIgnored) {
      getLogger().warning("Ignored: " + configPath.toAbsolutePath());
      return;
    }
    try (LineNumberReader reader = new LineNumberReader(Files.newBufferedReader(configPath))) {
      String line;
      while ((line = reader.readLine()) != null) {
        line = line.trim();
        if (line.isEmpty() || line.startsWith("#")) {
          continue;
        }
        final String[] params = StringUtils.split(line);
        if (params.length < 3) {
          throw new InvalidFileFormatException(String.format(
              "%1s:%2s: Not enough parameters (needs 3)",
              configPath.toRealPath().toString(),
              reader.getLineNumber()
          ));
        }
        final String launcherPath = params[0].startsWith("/")
            ? params[0].substring(1)
            : params[0];
        installLauncher(
            determineInstallDirectory(pkg).resolve(Paths.get(launcherPath)),
            params[1],
            params[2]
        );
      }
    }
  }

  private void installLauncher(final Path path,
                               final String mainClass,
                               final String artifact)
      throws IOException, DependencyResolutionException {
    getLogger().info("Resolving " + artifact);
    final String classpath = getResolver()
        .resolveAllDependencies(new DefaultArtifact(artifact), getRepositories())
        .parallelStream()
        .map(it -> getResolver().resolveArtifactPath(it.getArtifact(), it.getRepository()))
        .map(it -> Paths.get("/").resolve(getRepoPath()).resolve(it).toString())
        .sorted()
        .collect(Collectors.joining(":"));
    final List<String> launcher = IOUtils
        .readLines(getClass().getResourceAsStream("launcher.sh"), StandardCharsets.UTF_8)
        .stream()
        .map(it -> it.replace("@MAINCLASS@", mainClass))
        .map(it -> it.replace("@CLASSPATH@", classpath))
        .collect(Collectors.toList());
    getLogger().info("Writing launcher " + path);
    if (getDhArgs().isNoAct()) {
      return;
    }
    Files.createDirectories(path.getParent());
    FileUtils.writeLines(path.toFile(), launcher);
    Files.setPosixFilePermissions(
        path,
        EnumSet.of(
            PosixFilePermission.GROUP_EXECUTE,
            PosixFilePermission.GROUP_READ,
            PosixFilePermission.OTHERS_EXECUTE,
            PosixFilePermission.OTHERS_READ,
            PosixFilePermission.OWNER_EXECUTE,
            PosixFilePermission.OWNER_READ,
            PosixFilePermission.OWNER_WRITE
        )
    );
  }
}