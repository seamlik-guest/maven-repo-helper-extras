/*
 * Copyright 2018 殷啟聰 | Kai-Chung Yan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debian.maven;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.debian.debhelper.CommandResult;
import org.debian.debhelper.DebhelperSharedArgs;
import org.debian.debhelper.DebhelperUtils;
import org.eclipse.aether.repository.RemoteRepository;

/**
 * Provides common methods for the entry points.
 */
public abstract class Mh {

  /**
   * Common args aside {@link DebhelperSharedArgs}.
   */
  @Parameters(separators = "=")
  public class Args {

    @Parameter(names = { "-h", "-?", "--help" }, description = "Show help.", help = true)
    private boolean help;

    @Parameter(
        names = "--repo",
        description = "Specify the Maven repository of both the host system and the package"
            + "content to search."
    )
    private String repo = DebhelperUtils.DEFAULT_MAVEN_REPO_PATH.toString();

    public boolean isHelp() {
      return help;
    }

    public String getRepo() {
      return repo;
    }
  }

  private static final Collection<String> TRADITIONAL_PARAMS = Collections.unmodifiableCollection(
      Arrays.asList(
          "-N",
          "-P",
          "-p"
      )
  );

  private final DebhelperSharedArgs dhArgs = new DebhelperSharedArgs();
  private final Args commonArgs = new Args();
  private final Logger logger = Logger.getLogger(getClass().getCanonicalName());
  private DependencyResolver resolver;
  private Path sourceRoot;
  private Path repoPath = DebhelperUtils.DEFAULT_MAVEN_REPO_PATH;
  private List<String> packagesToActOn;
  private List<RemoteRepository> repositories;
  private RemoteRepository systemRepository;
  private Set<Path> ignoredPaths;

  private static List<String> processTraditionalArgs(final List<String> args) {
    // JCommander does not support the following flavors:
    //   * Single letter: -Pvalue (means -P value)
    // This function turns them into whatever JCommander recognizes.

    if (args.isEmpty()) {
      return args;
    }

    final List<String> result = new LinkedList<>();
    for (String arg : args) {
      final boolean hasSingleLetterOption = TRADITIONAL_PARAMS
          .parallelStream()
          .filter(it -> it.length() == 2)  // "-P" has length 2
          .anyMatch(it -> it.equals(arg.substring(0, 2)));
      if (hasSingleLetterOption) {
        result.add(arg.substring(0, 2));
        result.add(arg.substring(2));
        continue;
      }
      result.add(arg);
    }
    return result;
  }

  private void initializeMavenResolver() throws IOException {
    resolver = new DependencyResolver(
        findSourceRoot().resolve("debian/.mh/extras/repository")
    );
    systemRepository = new RemoteRepository.Builder(
        null,
        "default",
        "file:///" + repoPath.toString()
    ).build();
    repositories = new ArrayList<>(packagesToActOn.size());
    for (String pkg : packagesToActOn) {
      final Path packageRepoPath = determineInstallDirectory(pkg).resolve(repoPath);
      if (Files.notExists(packageRepoPath)) {
        continue;
      }
      repositories.add(new RemoteRepository.Builder(
          pkg,
          "default",
          "file://" + packageRepoPath.toRealPath()
      ).build());
    }
    repositories.add(systemRepository);
    repositories = Collections.unmodifiableList(repositories);
  }

  protected DebhelperSharedArgs getDhArgs() {
    return dhArgs;
  }

  protected DependencyResolver getResolver() {
    return resolver;
  }

  protected Logger getLogger() {
    return logger;
  }

  protected List<String> determinePackagesToActOn() {
    return packagesToActOn;
  }

  protected List<RemoteRepository> getRepositories() {
    return repositories;
  }

  protected Path findSourceRoot() throws IOException {
    if (sourceRoot == null) {
      sourceRoot = DebhelperUtils.findDebianSourceTreeRoot(Paths.get(".").toRealPath());
    }
    return sourceRoot;
  }

  protected Path getRepoPath() {
    return repoPath;
  }

  protected Args getCommonArgs() {
    return commonArgs;
  }

  protected RemoteRepository getSystemRepository() {
    return systemRepository;
  }

  protected abstract void run();

  protected void start(final String[] args) {
    final JCommander jcommander = JCommander
        .newBuilder()
        .addObject(dhArgs)
        .addObject(commonArgs)
        .build();

    List<String> dhArgsList = Stream
        .of(args)
        .filter(it -> it.startsWith("-O"))
        .map(it -> it.substring(2))
        .filter(StringUtils::isNotBlank)
        .collect(Collectors.toList());
    dhArgsList = processTraditionalArgs(dhArgsList);
    List<String> directArgList = Stream
        .of(args)
        .filter(it -> !it.startsWith("-O"))
        .collect(Collectors.toList());
    directArgList = processTraditionalArgs(directArgList);

    if (dhArgsList.size() > 0) {
      jcommander.setAcceptUnknownOptions(true);
      JCommander
          .newBuilder()
          .addObject(dhArgs)
          .addObject(commonArgs)
          .acceptUnknownOptions(true)
          .build()
          .parse(dhArgsList.toArray(new String[dhArgsList.size()]));
    }
    jcommander.setAcceptUnknownOptions(false);
    jcommander.parse(directArgList.toArray(new String[directArgList.size()]));

    if (commonArgs.isHelp()) {
      showHelp();
    }

    logger.setLevel(dhArgs.isVerbose() ? Level.ALL : Level.WARNING);
    try {

      final String repoPathText = commonArgs.repo.startsWith("/")
          ? commonArgs.repo.substring(1)
          : commonArgs.repo;
      repoPath = Paths.get(repoPathText);

      packagesToActOn = Collections.unmodifiableList(DebhelperUtils.listPackages(
          findSourceRoot(),
          dhArgs.isArch(),
          dhArgs.isIndep(),
          dhArgs.getExcludedPackages(),
          dhArgs.getIncludedPackages()
      ));
      initializeMavenResolver();
    } catch (Exception ex) {
      logger.log(Level.SEVERE, null, ex);
      System.exit(1);
    }

    run();
  }

  protected void showHelp() {
    JCommander.newBuilder().addObject(new Args()).programName(getProgramName()).build().usage();
    System.out.println("This program also accepts all shared debhelper options, although not "
        + "all are supported. For more information, see the manual of debhelper."
    );
    System.exit(0);
  }

  protected String getProgramName() {
    if (getClass().isAnnotationPresent(ProgramName.class)) {
      return getClass().getAnnotation(ProgramName.class).value();
    } else {
      return "";
    }
  }

  /**
   * Determines the main package after consulting command line options and environment variables.
   * @throws IOException If fails to run {@code dh_listpackages}.
   */
  protected String determineMainPackage()
      throws IOException, InterruptedException {
    if (!getDhArgs().getMainPackage().isEmpty()) {
      return getDhArgs().getMainPackage();
    } else {
      final CommandResult result = DebhelperUtils.runCommand(
          findSourceRoot(),
          Collections.singletonList("dh_listpackages")
      );
      if (result.getExitCode() != 0) {
        throw new FileNotFoundException(
            result.getOutput().stream().collect(Collectors.joining(System.lineSeparator()))
        );
      }
      return result.getOutput().get(0);
    }
  }

  /**
   * Determines the package installation directory after consulting command line options and
   * environment variables. Default should be {@code debian/<package>}.
   */
  protected Path determineInstallDirectory(final String pkg) throws IOException {
    if (dhArgs.getTmpdir() != null) {
      return dhArgs.getTmpdir();
    } else {
      return findSourceRoot().resolve("debian/" + pkg);
    }
  }

  protected Set<Path> findAllIgnoredFiles() throws IOException {
    if (ignoredPaths != null) {
      return ignoredPaths;
    }

    final Set<Path> result = new LinkedHashSet<>();
    for (String pattern : dhArgs.getIgnored()) {
      try (Stream<Path> stream = Files.find(
          Paths.get("."),
          Integer.MAX_VALUE,
          (path, attributes) -> {
            final boolean matches = path
                .getFileSystem()
                .getPathMatcher("glob:" + pattern)
                .matches(path.getNameCount() > 1 ? path.subpath(1, path.getNameCount()) : path);
            return attributes.isRegularFile() && matches;
          }
      )) {
        result.addAll(stream.collect(Collectors.toList()));
      }
    }
    ignoredPaths = Collections.unmodifiableSet(result);
    return ignoredPaths;
  }
}