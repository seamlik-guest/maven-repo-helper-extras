/*
 * Copyright 2018 殷啟聰 | Kai-Chung Yan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debian.maven;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.lang3.StringUtils;
import org.debian.debhelper.DebhelperUtils;
import org.debian.debhelper.InvalidFileFormatException;
import org.debian.debhelper.SubstvarsUtils;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.collection.DependencyCollectionException;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.util.artifact.ArtifactIdUtils;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * Main entry point for {@code mh_shlibdeps}.
 */
@ProgramName("mh_shlibdeps")
public class Shlibdeps extends Mh {

  /**
   * Entry point.
   */
  public static void main(final String[] args) {
    new Shlibdeps().start(args);
  }

  @Override
  protected void run() {
    try {
      for (String pkg : determinePackagesToActOn()) {
        processPackage(pkg);
      }
    } catch (Exception ex) {
      getLogger().log(Level.SEVERE, null, ex);
      System.exit(1);
    }
  }

  private void processPackage(final String pkg) throws
      IOException,
      DependencyCollectionException,
      ArtifactResolutionException,
      InvalidFileFormatException,
      InterruptedException {
    getLogger().info("Processing " + pkg);

    final Set<String> allResolvedPackages = new LinkedHashSet<>();
    for (Artifact installedArtifact : findAllArtifacts(pkg)) {
      getLogger().info("Resolving " + ArtifactIdUtils.toId(installedArtifact));
      final Collection<ArtifactResult> results = getResolver().resolveDirectDependencies(
          installedArtifact,
          getRepositories()
      );

      // Artifacts from the system
      final Collection<Artifact> externalArtifacts = results
          .parallelStream()
          .filter(it -> StringUtils.isBlank(it.getRepository().getId()))
          .map(ArtifactResult::getArtifact)
          .collect(Collectors.toList());
      for (Artifact resolvedArtifact : externalArtifacts) {
        final String resolvedPackage = findPackage(resolvedArtifact);
        if (resolvedPackage != null && !resolvedPackage.equals(pkg)) {
          allResolvedPackages.add(resolvedPackage);
        }
      }

      // Artifacts from this source package
      results
          .stream()
          .filter(it -> StringUtils.isNotBlank(it.getRepository().getId()))
          .filter(it -> !pkg.equals(it.getRepository().getId()))
          .forEach(it -> allResolvedPackages.add(it.getRepository().getId()));
    }
    processSubstvars(pkg, allResolvedPackages);
  }

  private void processSubstvars(final String pkg, final Set<String> dependencies)
      throws InvalidFileFormatException, IOException, InterruptedException {
    final Path namelessSubstvarsPath = findSourceRoot().resolve("debian").resolve("substvars");
    final Path substvarsPath;
    if (pkg.equals(determineMainPackage()) && Files.exists(namelessSubstvarsPath)) {
      substvarsPath = findSourceRoot().resolve("debian").resolve("substvars");
    } else {
      substvarsPath = findSourceRoot().resolve("debian").resolve(pkg + ".substvars");
    }
    final Map<String, String> substvars;
    try {
      substvars = Files.exists(substvarsPath)
          ? SubstvarsUtils.read(new FileReader(substvarsPath.toFile()))
          : new LinkedHashMap<>();
    } catch (FileNotFoundException ex) {
      throw new RuntimeException(ex);
    }

    final Set<String> items;
    if (substvars.containsKey("maven:Depends")) {
      items = Stream
          .of(substvars.get("maven:Depends").split(","))
          .map(String::trim)
          .filter(it -> !it.isEmpty())
          .collect(Collectors.toSet());
    } else {
      items = new LinkedHashSet<>();
    }
    items.addAll(dependencies);
    substvars.put(
        "maven:Depends",
        items.stream().sorted().collect(Collectors.joining(","))
    );

    if (!getDhArgs().isNoAct()) {
      SubstvarsUtils.write(substvars, new FileWriter(substvarsPath.toFile()));
    }
  }

  private String findPackage(final Artifact artifact) throws IOException, InterruptedException {
    final Path artifactPath = getResolver().resolveArtifactPath(artifact, getSystemRepository());
    final Set<String> allResolvedPackages = DebhelperUtils.findPackageUsingDpkg(
        Paths.get("/").resolve(getRepoPath()).resolve(artifactPath).toString()
    );
    if (allResolvedPackages.size() == 0) {
      throw new FileNotFoundException(String.format(
          "Failed to find any package containing `%1s`.",
          ArtifactIdUtils.toId(artifact)
      ));
    } else if (allResolvedPackages.size() != 1) {
      getLogger().warning(String.format(
          "Found multiple packages containing `%1s`, it is up to you to pick one.",
          ArtifactIdUtils.toId(artifact)
      ));
      return null;
    } else {
      return allResolvedPackages.iterator().next();
    }
  }

  private Set<Path> findAllPoms(final String pkg) throws IOException {
    final Path packageRepoPath = determineInstallDirectory(pkg).resolve(getRepoPath());
    if (!Files.isDirectory(packageRepoPath)) {
      return Collections.emptySet();
    }
    try (Stream<Path> paths = Files.find(
        packageRepoPath,
        Integer.MAX_VALUE,
        (path, attributes) -> attributes.isRegularFile()
            && path.getFileSystem().getPathMatcher("glob:**/*.pom").matches(path)
    )) {
      return paths.collect(Collectors.toSet());
    }
  }

  private Artifact convertPomToArtifact(final Path pomPath)
      throws IOException, InvalidFileFormatException {
    final String pomName = pomPath.toRealPath().toString();
    getLogger().info("Reading POM: " + pomName);
    final DocumentBuilder builder;
    try {
      builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
    try {
      final Element root = builder.parse(pomPath.toFile()).getDocumentElement();

      final String packaging = root
          .getElementsByTagName("packaging")
          .item(0)
          .getTextContent()
          .trim();
      if ("pom".equals(packaging)) {
        throw new IllegalArgumentException(pomName + ": Referring to a parent POM.");
      }

      final String groupId = root
          .getElementsByTagName("groupId")
          .item(0)
          .getTextContent()
          .trim();
      if (groupId.isEmpty()) {
        throw new IllegalArgumentException(pomName + ": Group ID absent.");
      }

      final String artifactId = root
          .getElementsByTagName("artifactId")
          .item(0)
          .getTextContent()
          .trim();
      if (artifactId.isEmpty()) {
        throw new IllegalArgumentException(pomName + ": Artifact ID absent.");
      }

      final String version = root
          .getElementsByTagName("version")
          .item(0)
          .getTextContent()
          .trim();
      if (version.isEmpty()) {
        throw new IllegalArgumentException(pomName + ": Version absent.");
      }

      return new DefaultArtifact(groupId, artifactId, packaging, version);
    } catch (SAXException ex) {
      throw new InvalidFileFormatException(pomName + ": Broken document.", ex);
    }
  }

  private Set<Artifact> findAllArtifacts(final String pkg) throws IOException {
    final Set<Artifact> results = new LinkedHashSet<>();
    for (Path path : findAllPoms(pkg)) {
      final Artifact artifact;
      try {
        artifact = convertPomToArtifact(path);
      } catch (Exception ex) {
        getLogger().log(Level.WARNING, null, ex);
        continue;
      }
      if (results.parallelStream().noneMatch(it -> ArtifactIdUtils.equalsId(it, artifact))) {
        results.add(artifact);
      }
    }
    return results;
  }
}