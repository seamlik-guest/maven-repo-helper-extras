#!/usr/bin/perl

use warnings;
use strict;
use Debian::Debhelper::Dh_Lib;

# Applying maven-repo-helper
use Debian::Debhelper::Sequence::maven_repo_helper;

# dh $@ --with maven_repo_helper_extras
insert_after("mh_install", "mh_shlibdeps");
insert_after("mh_shlibdeps, mh_genlaunchers");

1;