/*
 * Copyright 2018 殷啟聰 | Kai-Chung Yan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debian.debhelper;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SubstvarsUtilsTests {

  @Test
  void readTest() throws InvalidFileFormatException, IOException {
    final Map<String, String> substvars = SubstvarsUtils.read(new InputStreamReader(
        getClass().getResourceAsStream("valid.substvars")
    ));
    Assertions.assertEquals(
        "debhelper javahelper maven-repo-helper",
        substvars.get("misc:Depends")
    );
    Assertions.assertEquals("", substvars.get("maven:Depends"));
    Assertions.assertEquals(2, substvars.size());

    Assertions.assertThrows(
        InvalidFileFormatException.class,
        () -> SubstvarsUtils.read(new InputStreamReader(
            getClass().getResourceAsStream("duplicated-keys.substvars")
        ))
    );
    Assertions.assertThrows(
        InvalidFileFormatException.class,
        () -> SubstvarsUtils.read(new InputStreamReader(
            getClass().getResourceAsStream("leading-equal-sign.substvars")
        ))
    );
  }

  @Test
  void writeTest() throws IOException {
    final Map<String, String> data = new LinkedHashMap<>();
    data.put("maven:Depends", "javahelper");
    data.put("misc:Depends", "debhelper");
    final StringWriter writer = new StringWriter();
    SubstvarsUtils.write(data, writer);
    final String output = writer.toString();
    final List<String> outputLines = IOUtils.readLines(new StringReader(output));

    final List<String> samples = IOUtils.readLines(
        getClass().getResourceAsStream("write.substvars"),
        StandardCharsets.UTF_8
    );
    Assertions.assertEquals(samples, outputLines);
  }
}