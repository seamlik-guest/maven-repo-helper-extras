/*
 * Copyright 2018 殷啟聰 | Kai-Chung Yan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debian.maven;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.debian.debhelper.DebhelperUtils;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.DependencyResolutionException;
import org.eclipse.aether.util.artifact.ArtifactIdUtils;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

@Tag("human")
class HumanInterpretedTests {

  @ParameterizedTest
  @ValueSource(strings = "org.apache.maven:maven-resolver-provider:3.x")
  void resolveAll(final String query) throws IOException, DependencyResolutionException {
    final DependencyResolver resolver = new DependencyResolver(
        Paths.get("/tmp/maven-repo-helper-extras/repository")
    );
    System.out.println(String.format(
        "All dependencies of `%1s`:",
        query
    ));
    final Collection<ArtifactResult> result = resolver.resolveAllDependencies(
        new DefaultArtifact(query),
        Collections.singletonList(new RemoteRepository.Builder(
            null,
            "default",
            "file:///" + DebhelperUtils.DEFAULT_MAVEN_REPO_PATH
        ).build())
    );
    final List<String> output = new ArrayList<>(result.size());
    for (ArtifactResult it : result) {
      output.add(String.format(
          "    %1s -> %2s",
          ArtifactIdUtils.toId(it.getArtifact()),
          resolver.resolveArtifactPath(it.getArtifact(), it.getRepository())
      ));
    }
    output.stream().sorted().forEach(System.out::println);
  }

  @ParameterizedTest
  @ValueSource(strings = "org.apache.maven:maven-resolver-provider:3.x")
  void resolveDirect(final String query) throws Exception {
    final DependencyResolver resolver = new DependencyResolver(
        Paths.get("/tmp/maven-repo-helper-extras/repository")
    );
    System.out.println(String.format(
        "Direct dependencies of `%1s`:",
        query
    ));
    final Collection<ArtifactResult> result = resolver.resolveDirectDependencies(
        new DefaultArtifact(query),
        Collections.singletonList(new RemoteRepository.Builder(
            null,
            "default",
            "file:///" + DebhelperUtils.DEFAULT_MAVEN_REPO_PATH
        ).build())
    );
    final List<String> output = new ArrayList<>(result.size());
    for (ArtifactResult it : result) {
      output.add(String.format(
          "    %1s -> %2s",
          ArtifactIdUtils.toId(it.getArtifact()),
          resolver.resolveArtifactPath(it.getArtifact(), it.getRepository())
      ));
    }
    output.stream().sorted().forEach(System.out::println);
  }
}